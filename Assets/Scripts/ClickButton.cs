﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class ClickButton : MonoBehaviour
{
    public TextMeshProUGUI textMoney;
    public int money = 0;
    public float moneyTimer = 3f;
    public float currentTimer;
    public bool startTimer;
    public Slider countdownSlider;

    public void Start()
    {
        startTimer = true;
        money--;
    }

    public void Update()
    {
        if (startTimer == true)
        {
            currentTimer += Time.deltaTime;
            countdownSlider.value = currentTimer;
            if (currentTimer > moneyTimer)
            {
                startTimer = false;
                AddMoney();
                currentTimer = 0f;
            }
        }
    }

    public void AddMoney()
    {
        money++;
        textMoney.text = money.ToString();
    }
    public void StoreOnClick()
    {
        if (!startTimer)
        {
            startTimer = true;
        }
    }
    
}
